# MondoBrain

`@mondobrain/core` is a JavaScript wrapper of MondoBrain's central API

## Installation
```bash
yarn add @mondobrain/core
npm install @mondobrain/core
```

## Usage

```javascript
import { Client } from '@mondobrain/core'

// Node.JS users can use `@mondobrain/node#getAccessToken()` to get an access token
const token = "someaccesstokenissuedfromauth0"

const client = new Client(token);

ok = await client.api.test();
err = await client.api.test({ error: "Echo'ed error" });

const data = [
  { "engagement": 10.97, "eventtype": "Mission", "eventdurationmin": 117, "timespent": 46, "timespentmin": 53.82 },
  ...
  { "engagement": 11.88, "eventtype": "Mission", "eventdurationmin": 110, "timespent": 50, "timespentmin": 55 }
]

const opts = { data, outcome: 'engagement', target: 'max' };

const task = await client.solve.start(opts);

const result = await client.solve.result({ id: task.id });
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
