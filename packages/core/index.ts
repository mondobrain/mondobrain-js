import axios from "axios";
import type { AxiosError, AxiosInstance, AxiosRequestConfig } from "axios";
import pRetry from "p-retry";

export class BaseClient {
  public readonly apiUrl: string;

  public readonly token: string;

  private axios: AxiosInstance;

  constructor(
    token: string,
    { apiUrl = "https://api.datawhys.ai", headers = {} }: BaseClientOptions = {}
  ) {
    this.token = token;

    this.apiUrl = apiUrl;

    this.axios = axios.create({
      baseURL: this.apiUrl,
      headers: headers,
      maxRedirects: 0,
      proxy: false,
    });

    console.debug("initialized");
  }

  public async apiCall(
    method: string,
    options?: Base.Options
  ): Promise<Base.Response> {
    console.debug(`apiCall(${method}) start`);

    if (
      typeof options === "string" ||
      typeof options === "number" ||
      typeof options === "boolean"
    ) {
      throw new TypeError(
        `Expected an options argument but instead received a ${typeof options}`
      );
    }

    const response = await this.makeRequest(method, options);

    const result = response.data;
    return result;
  }

  public readonly api = {
    test: this.apiCall.bind(this, "api.test") as Base.Method<
      Api.TestOptions,
      Api.TestResponse
    >,
  };

  public readonly auth = {
    test: this.apiCall.bind(this, "auth.test") as Base.Method<
      Auth.TestOptions,
      Auth.TestResponse
    >,
  };

  public readonly language = {
    entities: this.apiCall.bind(this, "language.entities") as Base.Method<
      Language.EntitiesOptions,
      Language.EntitiesResponse
    >,
  };

  public readonly solve = {
    start: this.apiCall.bind(this, "solve.start") as Base.Method<
      Solve.StartOptions,
      Solve.StartResponse
    >,
    result: this.apiCall.bind(this, "solve.result") as Base.Method<
      Solve.ResultOptions,
      Solve.ResultResponse
    >,
  };

  private async makeRequest(
    url: string,
    body: unknown,
    config: AxiosRequestConfig = {}
  ) {
    const task = async () => {
      try {
        config = {
          ...config,
          headers: {
            ...config.headers,
            Authorization: `Bearer ${this.token}`,
          },
        };

        const response = await this.axios.post(url, body, config);

        if (response.status === 202) {
          const retrySec = 2;
          await delay(retrySec * 1000);
          throw Error("Reulst was not yet ready");
        }

        return response;
      } catch (err) {
        if (isAxiosError(err) && err.response) {
          if (
            isRequestError(err.response.data) ||
            (err.response.status >= 400 && err.response.status <= 499)
          ) {
            const data = err.response.data;
            throw new pRetry.AbortError(newRequestError(data));
          }
        }

        // All other error types are rethrown so that they can be retried
        throw err;
      }
    };

    return pRetry(task, { factor: 1.9793, maxTimeout: 20 * 1000 });
  }
}

export function newRequestError(e: Base.Error) {
  return new RequestError(e);
}

function isRequestError(data: any): data is Base.Error {
  return typeof data.ok !== "undefined" && !data.ok;
}

/**
 * Delay returns a Promise that will resolve after the specified number of milliseconds.
 * @param ms milliseconds to wait
 * @param value value for eventual resolution
 * @returns Promise that resolves after the specified number of milliseconds
 */
function delay<T>(ms: number, value?: T): Promise<T> {
  return new Promise((resolve) => {
    setTimeout(() => resolve(value), ms);
  });
}

//////////////////////////
// TYPES
//////////////////////////
export type BaseClientOptions = {
  apiUrl?: string;
  headers?: object;
  maxRequestConcurrency?: number;
};

/**
 * ContinuousFeature represents numeric/regressive data
 */
type ContinuousFeature = {
  key: string;
  type: "continuous";
  range: [number, number];
};

/**
 * DiscreteFeature represents class data
 */
type DiscreteFeature = { key: string; type: "discrete"; modalities: string[] };

/**
 * A Feature can either be discrete or continuous
 */
export type Feature = DiscreteFeature | ContinuousFeature;

/**
 * Shape follows numpy conventions for shape - (height, width)
 */
export type Shape = [number, number];

/**
 * ContinuousFilter represents a filter for numeric/regressive data.
 */
export type ContinuousFilter = {
  lo: number;
  hi: number;
};

/**
 * DiscreteFilter represents a filter for categorical data.
 */
export type DiscreteFilter = string;

/**
 * A Rule can be either a continuous or discrete.
 */
export type Filter = ContinuousFilter | DiscreteFilter;

export type Rule = {
  [key: string]: Filter;
};

//////////////////////////
// API Request/Response Types
//////////////////////////
export class RequestError extends Error {
  constructor(err: Base.Error) {
    let message = `${err.message} - type: ${err.type}`;
    if (err.code) {
      message += ` - code: ${err.code}`;
    }

    super(message);
    this.name = "RequestError";
  }
}

export namespace Base {
  export interface Response {
    ok: boolean;
  }

  export interface Error extends Response {
    ok: false;
    type: string;
    code?: string;
    message: string;
  }

  export interface Options {
    [argument: string]: unknown;
  }

  export interface Method<O extends Options, R extends Response> {
    (options?: O): Promise<R>;
  }
}
export namespace Api {
  export interface TestOptions extends Base.Options {
    error?: string;
  }

  export interface TestResponse extends Base.Response {
    error?: string;
  }
}

export namespace Auth {
  export interface TestOptions extends Base.Options {
    error?: string;
  }

  export interface TestResponse extends Base.Response {
    error?: string;
  }
}

export namespace Language {
  export interface EntitiesMetadata {
    features: Feature[];
    shape: Shape;
  }

  export interface EntitiesOptions extends Base.Options {
    message: string;
    metadata: EntitiesMetadata;
  }

  export interface EntitiesResponse extends Base.Response {
    entities: {
      explorable: string[];
      outcome: string;
      operation: {
        type: "continuous" | "discrete";
        value: string;
      };
      constraints: string[];
    };
  }
}

export namespace Solve {
  export interface StartOptions extends Base.Options {
    outcome: string;
    target: string;
    data: object[];
  }

  export interface StartResponse extends Base.Response {
    id: string;
  }

  export interface ResultOptions extends Base.Options {
    id: string;
  }

  export interface ResultResponse extends Base.Response {
    rule: Rule;
  }
}

//////////////////////////
// TYPE GUARDS
//////////////////////////
function isAxiosError(err: any): err is AxiosError {
  return !!err.request || !!err.response;
}

export function isDiscreteFilter(filter: Filter): filter is DiscreteFilter {
  return typeof filter === "string";
}

export function isContinuousFilter(filter: Filter): filter is ContinuousFilter {
  return !isDiscreteFilter(filter);
}
