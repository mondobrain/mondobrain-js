# MondoBrain

`@mondobrain/node` is a group of MondoBrain JS features specifically for Node.JS

## Installation
```bash
yarn add @mondobrain/node
npm install @mondobrain/node
```

## Usage

```javascript
import { getAccessToken } from '@mondobrain/node';

await getAccessToken(clientID, clientSecret);
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
