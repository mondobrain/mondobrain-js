import fetch from "node-fetch";

/**
 * getAccessToken gets an access token from the Auth0 API based on the client ID and client secret.
 * @param clientID The client ID of the application.
 * @param clientSecret The client secret of the application.
 */
export async function getAccessToken(
  id: string,
  secret: string,
  {
    domain = "id.datawhys.ai",
    audience = "https://api.mondobrain.com/",
  }: AuthOptions = {}
) {
  const creds: Auth0Credentials = {
    client_id: id,
    client_secret: secret,
    audience: audience,
    grant_type: "client_credentials",
  };

  const resp = await fetch(`https://${domain}/oauth/token`, {
    method: "POST",
    body: JSON.stringify(creds),
    headers: {
      "content-type": "application/json",
    },
  });

  const tokenInfo: Auth0Token = await resp.json();

  if (tokenInfo.error) {
    const errType = tokenInfo.error || "auth_error";
    const errDesc = tokenInfo.error_description || "unknown reason";
    const errMessage = `${errType}: ${errDesc}`;
    throw new Error(errMessage);
  }

  return tokenInfo.access_token;
}

//////////////////////////
// TYPES
//////////////////////////
export type AuthOptions = {
  domain?: string;
  audience?: string;
};

type Auth0Credentials = {
  client_id: string;
  client_secret: string;
  audience: string;
  grant_type: string;
};

type Auth0Token = {
  access_token: string;
  scope: string;
  expires_in: number;
  token_type: string;
  error?: string;
  error_description?: string;
};
