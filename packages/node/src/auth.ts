import fetch from 'node-fetch';

export interface AuthOptions {
  domain?: string;
  audience?: string;
}

interface Auth0Credentials {
  client_id: string;
  client_secret: string;
  audience: string;
  grant_type: string;
}

interface Auth0Token {
  access_token: string;
  scope: string;
  expires_in: number;
  token_type: string;
  error?: string;
  error_description?: string;
}

export async function getAccessToken(
  clientID: string,
  clientSecret: string,
  { domain = 'id.datawhys.ai', audience = 'https://api.mondobrain.com/' }: AuthOptions = {},
): Promise<string> {
  /* eslint-disable @typescript-eslint/camelcase */
  const creds: Auth0Credentials = {
    client_id: clientID,
    client_secret: clientSecret,
    audience: audience,
    grant_type: 'client_credentials',
  };
  /* eslint-enable @typescript-eslint/camelcase */

  const resp = await fetch(`https://${domain}/oauth/token`, {
    method: 'POST',
    body: JSON.stringify(creds),
    headers: {
      'content-type': 'application/json',
    },
  });

  const tokenInfo: Auth0Token = await resp.json();

  if (tokenInfo.error) {
    const eType = tokenInfo.error || 'auth_error';
    const desc = tokenInfo.error_description || 'unknown reason';
    const message = `${eType}: ${desc}`;
    throw new Error(message);
  }

  return tokenInfo.access_token;
}
